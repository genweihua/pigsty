# App

Pigsty Application

## Core Application


### PostgreSQL Monitoring System [`<pgsql/>`](pgsql/)

* Best PostgreSQL monitoring system ever!
* Visualize 1000+ metrics in dozens of dashboards

### Pigsty CMDB [`<cmdb/>`](cmdb/)

* Replace `pigsty.yml` config file with CMDB!
* Mange PostgreSQL clusters with PostgreSQL.


### PostgreSQL Catalog Explorer `<pgcat/>`

### PostgreSQL Logging Analyzer `<pgdog/>`

### PostgreSQL Explain Visualizer `<pgev/>`
