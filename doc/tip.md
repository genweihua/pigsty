# Cheatsheet


## Download

Download pigsty source From Github

```bash
VERSION=v0.9.1
https://github.com/Vonng/pigsty/releases/download/${VERSION}/pigsty.tgz
```

Download pigsty resource from Pan.Baidu

* [https://pan.baidu.com/s/1DZIa9X2jAxx69Zj-aRHoaw](https://pan.baidu.com/s/1DZIa9X2jAxx69Zj-aRHoaw)
* `8su9`


Download pigsty pkg from Github

```bash
https://github.com/Vonng/pigsty/releases/download/${VERSION}/pkg.tgz
```


Download pigsty source code from Official Site

```bash

```


Download pigsty pkg from CDN

```bash

```


Download pigsty pkg from Baidu NetDisk




## Install

```bash

```


## Playbooks

```bash
./pgsql.yml  -l  <cluster>     # init pgsql cluster <cluster>
```



## CLI Tools

```bash
```